package models

import "fmt"

type Photos struct {
	Id        string
	ImagePath string
}

// 返回数据 Photos
func GetPhotos() []Photos {
	photos := []Photos{
		{Id: "pic1", ImagePath: "img01.png"},
		{Id: "pic2", ImagePath: "img02.png"},
		{Id: "pic3", ImagePath: "img03.png"},
		{Id: "pic4", ImagePath: "img04.png"},
		{Id: "pic5", ImagePath: "img05.png"},
		{Id: "pic6", ImagePath: "img06.png"},
	}
	return photos
}

// 返回数据 Title
func GetTitle() string {
	return "隔壁王校长用于教学瞎编的标题：DEV"
}

// 返回数据 Price
func GetPrice() string {
	price := 139900
	intPrice := price / 100
	decimalPrice := price % 100
	return fmt.Sprintf("%d.%02d", intPrice, decimalPrice)
}
