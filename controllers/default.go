package controllers

import (
	"github.com/labstack/gommon/log"

	"beego.shopping.detail/models"
	"github.com/astaxie/beego"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) Get() {
	// 获取URI参数
	id := c.Ctx.Input.Param(":id")
	log.Printf("url :%v\n", id)
	if id == "" {
		c.Abort("404")
	}
	// 图片
	photos := models.GetPhotos()
	// 标题
	title := models.GetTitle()
	// 价格
	price := models.GetPrice()
	c.Data["Photos"] = photos
	c.Data["Title"] = title
	c.Data["Price"] = price
	c.TplName = "index.html"
}
