package routers

import (
	"beego.shopping.detail/controllers"
	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/detail/?:id", &controllers.MainController{})
}
