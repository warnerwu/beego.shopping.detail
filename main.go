package main

import (
	"beego.shopping.detail/controllers"
	_ "beego.shopping.detail/routers"
	"github.com/astaxie/beego"
)

func main() {
	beego.ErrorController(&controllers.ErrorController{})
	beego.Run()
}
